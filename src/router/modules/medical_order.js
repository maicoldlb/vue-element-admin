/** When your routing table is too long, you can split it into small modules **/

import Layout from '@/layout'

const medicalOrderRouter = {
  path: '/medical-order',
  component: Layout,
  redirect: '/medical-order/services',
  name: 'medical-order',
  meta: {
    title: 'ÓRDENES MÉDICAS',
    icon: 'skill'
  },
  children: [
    {
      path: 'services',
      component: () => import('@/views/table/dynamic-table/index'),
      name: 'services',
      meta: { title: 'Órdenes de procedimiento' }
    },
    {
      path: 'products',
      component: () => import('@/views/table/drag-table'),
      name: 'products',
      meta: { title: 'Órdenes de medicamentos' }
    },
    {
      path: 'orders',
      component: () => import('@/views/table/inline-edit-table'),
      name: 'orders',
      meta: { title: 'Órdenes médicas' }
    }
  ]
}
export default medicalOrderRouter
