/** When your routing table is too long, you can split it into small modules **/

import Layout from '@/layout'

const uciRouter = {
  path: '/uci',
  component: Layout,
  redirect: '/uci/admission',
  name: 'uci',
  meta: {
    title: 'UCI',
    icon: 'money'
  },
  children: [
    {
      path: 'admission',
      component: () => import('@/views/table/dynamic-table/index'),
      name: 'admission',
      meta: { title: 'Admisión' }
    },
    {
      path: 'patients-pending',
      component: () => import('@/views/table/drag-table'),
      name: 'patients-pending',
      meta: { title: 'Pacientes pendientes' }
    },
    {
      path: 'patients-uci',
      component: () => import('@/views/table/inline-edit-table'),
      name: 'patients-uci',
      meta: { title: 'Pacientes UCI' }
    }
  ]
}
export default uciRouter
