/** When your routing table is too long, you can split it into small modules **/

import Layout from '@/layout'

const imagesRouter = {
  path: '/images',
  component: Layout,
  redirect: '/images/image',
  name: 'images',
  meta: {
    title: 'IMÁGENES',
    icon: 'dashboard'
  },
  children: [
    {
      path: 'image',
      component: () => import('@/views/table/dynamic-table/index'),
      name: 'image',
      meta: { title: 'Imagén prueba' }
    },
    {
      path: 'report',
      component: () => import('@/views/table/dynamic-table/index'),
      name: 'report',
      meta: { title: 'Reportes' }
    }
  ]
}
export default imagesRouter
