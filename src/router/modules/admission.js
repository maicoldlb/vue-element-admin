/** When your routing table is too long, you can split it into small modules **/

import Layout from '@/layout'

const admissionRouter = {
  path: '/admission',
  component: Layout,
  redirect: '/admission/patients',
  name: 'Admission',
  meta: {
    title: 'ADMISIÓN',
    icon: 'peoples'
  },
  children: [
    {
      path: 'patients',
      component: () => import('@/views/table/dynamic-table/index'),
      name: 'patients',
      meta: { title: 'Pacientes' }
    },
    {
      path: 'appointments',
      component: () => import('@/views/table/drag-table'),
      name: 'appointment',
      meta: { title: 'Citas' }
    },
    {
      path: 'doctors',
      component: () => import('@/views/table/inline-edit-table'),
      name: 'doctors',
      meta: { title: 'Médicos' }
    },
    {
      path: 'reports',
      component: () => import('@/views/table/complex-table'),
      name: 'reports',
      meta: { title: 'Reports' }
    }
  ]
}
export default admissionRouter
