/** When your routing table is too long, you can split it into small modules **/

import Layout from '@/layout'

const InsuranceRouter = {
  path: '/insurance',
  component: Layout,
  redirect: '/insurance/fuas',
  name: 'Insurance',
  meta: {
    title: 'SEGUROS',
    icon: 'skill'
  },
  children: [
    {
      path: 'fuas',
      component: () => import('@/views/table/dynamic-table/index'),
      name: 'fuas',
      meta: { title: 'Gestión de FUAS' }
    },
    {
      path: 'sis',
      component: () => import('@/views/table/drag-table'),
      name: 'sis',
      meta: { title: 'Pago pendiente SIS' }
    },
    {
      path: 'soat',
      component: () => import('@/views/table/inline-edit-table'),
      name: 'soat',
      meta: { title: 'Pago pendiente SOAT' }
    }
  ]
}
export default InsuranceRouter
