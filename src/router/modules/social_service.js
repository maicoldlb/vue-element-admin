/** When your routing table is too long, you can split it into small modules **/

import Layout from '@/layout'

const socialServiceRouter = {
  path: '/social-service',
  component: Layout,
  redirect: '/social-service/box',
  name: 'social-service',
  meta: {
    title: 'SERVICIO SOCIAL',
    icon: 'money'
  },
  children: [
    {
      path: 'box',
      component: () => import('@/views/table/dynamic-table/index'),
      name: 'box',
      meta: { title: 'Bandeja' }
    },
    {
      path: 'discount',
      component: () => import('@/views/table/dynamic-table/index'),
      name: 'disocunt',
      meta: { title: 'Descuentos aprobandos' }
    }
  ]
}
export default socialServiceRouter
