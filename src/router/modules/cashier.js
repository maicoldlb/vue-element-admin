/** When your routing table is too long, you can split it into small modules **/

import Layout from '@/layout'

const cashierRouter = {
  path: '/cashier',
  component: Layout,
  redirect: '/cashier/cashier',
  name: 'cashier',
  meta: {
    title: 'CAJA',
    icon: 'money'
  },
  children: [
    {
      path: 'cashier',
      component: () => import('@/views/table/dynamic-table/index'),
      name: 'cashier',
      meta: { title: 'Caja' }
    },
    {
      path: 'patient-account',
      component: () => import('@/views/table/drag-table'),
      name: 'patient-account',
      meta: { title: 'Cuentas de paciente' }
    },
    {
      path: 'reprint',
      component: () => import('@/views/table/inline-edit-table'),
      name: 'reprint',
      meta: { title: 'Reimpresión' }
    },
    {
      path: 'reports',
      component: () => import('@/views/table/complex-table'),
      name: 'reports',
      meta: { title: 'Informes' }
    }
  ]
}
export default cashierRouter
