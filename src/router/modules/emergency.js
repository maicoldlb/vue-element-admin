/** When your routing table is too long, you can split it into small modules **/

import Layout from '@/layout'

const emergencyRouter = {
  path: '/emergency',
  component: Layout,
  redirect: '/emergency/patients',
  name: 'emergency',
  meta: {
    title: 'EMERGENCIA',
    icon: 'eye-open'
  },
  children: [
    {
      path: 'patients',
      component: () => import('@/views/table/dynamic-table/index'),
      name: 'patients',
      meta: { title: 'Pacientes' }
    },
    {
      path: 'admission',
      component: () => import('@/views/table/drag-table'),
      name: 'admission',
      meta: { title: 'Admisión' }
    },
    {
      path: 'evaluation',
      component: () => import('@/views/table/inline-edit-table'),
      name: 'doctors',
      meta: { title: 'Evaluación de emergencia' }
    },
    {
      path: 'egress',
      component: () => import('@/views/table/complex-table'),
      name: 'egress',
      meta: { title: 'Egreso' }
    }
  ]
}
export default emergencyRouter
