/** When your routing table is too long, you can split it into small modules **/

import Layout from '@/layout'

const settlementRouter = {
  path: '/settlement',
  component: Layout,
  redirect: '/settlement/sis',
  name: 'settlement',
  meta: {
    title: 'LIQUIDACIONES',
    icon: 'chart'
  },
  children: [
    {
      path: 'sis',
      component: () => import('@/views/table/dynamic-table/index'),
      name: 'sis',
      meta: { title: 'SIS' }
    },
    {
      path: 'soat',
      component: () => import('@/views/table/drag-table'),
      name: 'soat',
      meta: { title: 'SOAT' }
    },
    {
      path: 'payer',
      component: () => import('@/views/table/inline-edit-table'),
      name: 'payer',
      meta: { title: 'Particulares' }
    },
    {
      path: 'agreement',
      component: () => import('@/views/table/complex-table'),
      name: 'agreement',
      meta: { title: 'Convenios' }
    },
    {
      path: 'refund',
      component: () => import('@/views/table/complex-table'),
      name: 'refund',
      meta: { title: 'Reembolsos' }
    },
    {
      path: 'medical-audit',
      component: () => import('@/views/table/complex-table'),
      name: 'medical-audit',
      meta: { title: 'Auditoría médica' }
    },
    {
      path: 'patients-account',
      component: () => import('@/views/table/complex-table'),
      name: 'patients-account',
      meta: { title: 'Cuentas de pacientes' }
    }
  ]
}
export default settlementRouter
