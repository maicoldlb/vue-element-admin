/** When your routing table is too long, you can split it into small modules **/

import Layout from '@/layout'

const bedRouter = {
  path: '/bed',
  component: Layout,
  redirect: '/bed/managment-bed',
  name: 'bed',
  meta: {
    title: 'CAMAS',
    icon: 'people'
  },
  children: [
    {
      path: 'managment-bed',
      component: () => import('@/views/table/dynamic-table/index'),
      name: 'managment-bed',
      meta: { title: 'Gestión de cama' }
    },
    {
      path: 'report',
      component: () => import('@/views/table/dynamic-table/index'),
      name: 'report',
      meta: { title: 'Reportes' }
    }
  ]
}
export default bedRouter
