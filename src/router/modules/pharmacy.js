/** When your routing table is too long, you can split it into small modules **/

import Layout from '@/layout'

const pharmacyRouter = {
  path: '/pharmacy',
  component: Layout,
  redirect: '/pharmacy/prescription',
  name: 'pharmacy',
  meta: {
    title: 'FARMACIA',
    icon: 'shopping'
  },
  children: [
    {
      path: 'prescription',
      component: () => import('@/views/table/dynamic-table/index'),
      name: 'prescription',
      meta: { title: 'Prescripción' }
    },
    {
      path: 'sale',
      component: () => import('@/views/table/drag-table'),
      name: 'sale',
      meta: { title: 'Ventas' }
    },
    {
      path: 'purchase',
      component: () => import('@/views/table/inline-edit-table'),
      name: 'purchase',
      meta: { title: 'Compras' }
    },
    {
      path: 'stock',
      component: () => import('@/views/table/complex-table'),
      name: 'stock',
      meta: { title: 'Inventario' }
    },
    {
      path: 'report',
      component: () => import('@/views/table/complex-table'),
      name: 'report',
      meta: { title: 'Reportes' }
    }
  ]
}
export default pharmacyRouter
