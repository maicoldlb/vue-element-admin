/** When your routing table is too long, you can split it into small modules **/

import Layout from '@/layout'

const ceRouter = {
  path: '/external-consult',
  component: Layout,
  redirect: '/external-consult/appointments',
  name: 'external-consult',
  meta: {
    title: 'CONSULTA EXTERNA',
    icon: 'clipboard'
  },
  children: [
    {
      path: 'appointments',
      component: () => import('@/views/table/drag-table'),
      name: 'appointment',
      meta: { title: 'Citas' }
    },
    {
      path: 'evaluations',
      component: () => import('@/views/table/inline-edit-table'),
      name: 'evaluations',
      meta: { title: 'Evaluaciones' }
    }
  ]
}
export default ceRouter
