/** When your routing table is too long, you can split it into small modules **/

import Layout from '@/layout'

const hospitalizationRouter = {
  path: '/hospitalization',
  component: Layout,
  redirect: '/hospitalization/admission',
  name: 'hospitalization',
  meta: {
    title: 'HOSPITALIZACIÓN',
    icon: 'component'
  },
  children: [
    {
      path: 'admission',
      component: () => import('@/views/table/dynamic-table/index'),
      name: 'admission',
      meta: { title: 'Admisión' }
    },
    {
      path: 'patients-pending',
      component: () => import('@/views/table/drag-table'),
      name: 'patients-pending',
      meta: { title: 'Pacientes pendientes' }
    },
    {
      path: 'inpatients',
      component: () => import('@/views/table/inline-edit-table'),
      name: 'inpatients',
      meta: { title: 'Pacientes hospitalizados' }
    },
    {
      path: 'patients-discharge',
      component: () => import('@/views/table/complex-table'),
      name: 'patients-discharge',
      meta: { title: 'Alta médica' }
    },
    {
      path: 'transfers',
      component: () => import('@/views/table/complex-table'),
      name: 'transfers',
      meta: { title: 'Transferencias' }
    },
    {
      path: 'nursing',
      component: () => import('@/views/table/complex-table'),
      name: 'nursing',
      meta: { title: 'Enfermería' }
    }
  ]
}
export default hospitalizationRouter
