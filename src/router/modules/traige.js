/** When your routing table is too long, you can split it into small modules **/

import Layout from '@/layout'

const triageRouter = {
  path: '/triaje',
  component: Layout,
  redirect: '/triaje/patients',
  name: 'triaje',
  meta: {
    title: 'TRIAJE',
    icon: 'money'
  },
  children: [
    {
      path: 'patients',
      component: () => import('@/views/table/dynamic-table/index'),
      name: 'patients',
      meta: { title: 'Pacientes' }
    },
    {
      path: 'triaje',
      component: () => import('@/views/table/drag-table'),
      name: 'triaje',
      meta: { title: 'Triaje' }
    }
  ]
}
export default triageRouter
