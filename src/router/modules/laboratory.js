/** When your routing table is too long, you can split it into small modules **/

import Layout from '@/layout'

const laboratoryRouter = {
  path: '/laboratory',
  component: Layout,
  redirect: '/laboratory/pathology-clinic',
  name: 'laboratory',
  meta: {
    title: 'LABORATORIO',
    icon: 'skill'
  },
  children: [
    {
      path: 'pathology-clinic',
      component: () => import('@/views/table/dynamic-table/index'),
      name: 'pathology-clinic',
      meta: { title: 'Patología clínica' }
    },
    {
      path: 'pathology-anatomy',
      component: () => import('@/views/table/drag-table'),
      name: 'pathology-anatomy',
      meta: { title: 'Anatomía patología' }
    },
    {
      path: 'blood-bank',
      component: () => import('@/views/table/inline-edit-table'),
      name: 'blood-bank',
      meta: { title: 'Banco de sangre' }
    },
    {
      path: 'income',
      component: () => import('@/views/table/complex-table'),
      name: 'income',
      meta: { title: 'Ingreso de insumos' }
    },
    {
      path: 'outgoing',
      component: () => import('@/views/table/complex-table'),
      name: 'outgoing',
      meta: { title: 'Salida de insumos' }
    }
  ]
}
export default laboratoryRouter
