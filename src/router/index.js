import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

/* Layout */
import Layout from '@/layout'

/* Router Modules */
import admissionRouter from './modules/admission'
import bedRouter from './modules/beds'
import cashierRouter from './modules/cashier'
import emergencyRouter from './modules/emergency'
import ceRouter from './modules/external_consult'
import insuranceRouter from './modules/insurance'
import imagesRouter from './modules/images'
import laboratoryRouter from './modules/laboratory'
import medicalOrderRouter from './modules/medical_order'
import pharmacyRouter from './modules/pharmacy'
import settlementRouter from './modules/settlement'
import socialServiceRouter from './modules/social_service'
import triageRouter from './modules/traige'
import uciRouter from './modules/uci'
import hospitalizationRouter from './modules/hospitalization'

export const constantRoutes = [
  {
    path: '/redirect',
    component: Layout,
    hidden: true,
    children: [
      {
        path: '/redirect/:path*',
        component: () => import('@/views/redirect/index')
      }
    ]
  },
  {
    path: '/login',
    component: () => import('@/views/login/index'),
    hidden: true
  },
  {
    path: '/auth-redirect',
    component: () => import('@/views/login/auth-redirect'),
    hidden: true
  },
  {
    path: '/404',
    component: () => import('@/views/error-page/404'),
    hidden: true
  },
  {
    path: '/401',
    component: () => import('@/views/error-page/401'),
    hidden: true
  },
  {
    path: '/',
    component: Layout,
    redirect: '/sihce',
    children: [
      {
        path: 'sihce',
        component: () => import('@/views/dashboard/index'),
        name: 'sihce',
        meta: { title: 'SIHCE', icon: 'dashboard', affix: true }
      }
    ]
  }
]

export const asyncRoutes = [
  admissionRouter,
  bedRouter,
  cashierRouter,
  emergencyRouter,
  ceRouter,
  hospitalizationRouter,
  insuranceRouter,
  imagesRouter,
  laboratoryRouter,
  medicalOrderRouter,
  pharmacyRouter,
  settlementRouter,
  socialServiceRouter,
  triageRouter,
  uciRouter,
  { path: '*', redirect: '/404', hidden: true }
]

const createRouter = () => new Router({
  scrollBehavior: () => ({ y: 0 }),
  routes: constantRoutes
})

const router = createRouter()

export function resetRouter() {
  const newRouter = createRouter()
  router.matcher = newRouter.matcher // reset router
}

export default router
